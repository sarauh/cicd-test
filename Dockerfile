# ROM rocker/geospatial

FROM rocker/shiny-verse:latest

# system libraries of general use
## install debian packages
RUN apt-get update -qq && apt-get -y --no-install-recommends install \
    libxml2-dev \
    libcairo2-dev \
    libsqlite3-dev \
    libpq-dev \
    libssh2-1-dev \
    unixodbc-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    libgdal-dev \
    libproj-dev \
    libgeos-dev \
    libudunits2-dev \
    netcdf-bin \
    && rm -rf /var/lib/apt/lists/*
    
    

## update system libraries
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean

# copy necessary files
## renv.lock file
# COPY /R/renv.lock ./renv.lock
## app folder
#COPY /R ./app

# install renv & restore packages
# RUN Rscript -e 'install.packages("renv")'
# RUN Rscript -e 'renv::restore()'

#RUN R -e "install.packages('tidyverse',dependencies=TRUE, repos='http://cran.rstudio.com/')"

RUN R -e "install.packages('shiny',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('datasets',dependencies=TRUE, repos='http://cran.rstudio.com/')"

# RUN R -e "install.packages('Cairo',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('DT',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('data.table',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('dplyr',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('echarts4r',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('flextable',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('leaflet',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('openxlsx',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('sf',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('shinyWidgets',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('shinycssloaders',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('shinydashboard',dependencies=TRUE, repos='http://cran.rstudio.com/')"
# RUN R -e "install.packages('shinyjs',dependencies=TRUE, repos='http://cran.rstudio.com/')"



RUN mkdir /root/app

COPY R /root/shiny_save

# expose port
EXPOSE 3838

# run app on container start
CMD ["R", "-e", "shiny::runApp('/root/shiny_save', host='0.0.0.0', port=3838)"]
